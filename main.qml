import QtQuick 2.14
import QtQuick.Controls 2.14


ApplicationWindow {
    visible: true
    width: 1024
    height: 600
    title: qsTr("Sock Drawer")
    color: "#2F343F"


    SwipeView {
        id: swipeView
        anchors.fill: parent
        //currentIndex: tabBar.currentIndex

        Page1Form {
        }

        Page2Form {
        }
    }


    header: Row {
        id: searchRow
        topPadding: 20;
        leftPadding: 40;
        rightPadding: 40;
        height: 80;

        TextArea {
            id: searchArea;
            KeyNavigation.priority: KeyNavigation.BeforeItem
            KeyNavigation.tab: swipeView
            anchors.centerIn: parent
            renderType: TextInput.NativeRendering
            horizontalAlignment: TextInput.AlignHCenter


            background: Rectangle {
                radius: 6;
                border.color: searchArea.activeFocus ? "#5294e2" : "transparent";
                border.width: 3;
                implicitWidth: 520
                implicitHeight: 32
                Text {
                    visible: searchArea.text.length == 0 && !searchArea.activeFocus
                    anchors.centerIn: parent
                    text: "Search"
                }
            }
        }
    }

    footer: Row {
        id: radioRow
        Column {
            id: radioColumn
            Row {
                x: swipeView.width //- (radioColumn / 2)
                RadioButton {
                    checked: true
                }
                RadioButton {
                    checked: false
                }
            }
        }


    }
}
