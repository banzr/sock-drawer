import QtQuick 2.14
import QtQuick.Controls 2.14

Page {
    id: page

    background: Rectangle {
        color: "#2F343F"
    }

    GridView {
        //anchors.horizontalCenter: parent.horizontalCenter
        cellHeight: 120
        cellWidth: 120

        anchors.fill: parent

        anchors.leftMargin: 140
        anchors.rightMargin: 140
        anchors.topMargin: 40
        anchors.bottomMargin: 40
        anchors.centerIn: parent

        model: ContactModel {}
        delegate: Rectangle {

            Column {

                Image {
                    source: portrait
                    height: 64
                    width: 64
                    anchors.horizontalCenter: parent.horizontalCenter
                }
                Text {
                    topPadding: 8
                    text: name
                    color: "#ffffff"
                    anchors.horizontalCenter: parent.horizontalCenter
                }
            }
        }
    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
##^##*/

