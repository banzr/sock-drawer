# Sock Drawer

Application drawer for Linux. When paired with dmenu, helps visualize installed applications that may be forgotten.